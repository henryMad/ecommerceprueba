import { TestBed } from '@angular/core/testing';

import { InformacionEcommerceService } from './informacion-ecommerce.service';

describe('InformacionEcommerceService', () => {
  let service: InformacionEcommerceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InformacionEcommerceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
