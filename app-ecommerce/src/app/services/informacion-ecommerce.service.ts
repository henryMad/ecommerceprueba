import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Usuario } from '../interface/usuario';
import { RepuestaAutorizacion } from '../interface/usuario';
import { ListaDeProductos } from '../interface/usuario';

@Injectable({
  providedIn: 'root'
})
export class InformacionEcommerceService {

  urlAutorizacion: string = 'http://localhost:8080/usuario/autorizacion';
  urlCliente: string = 'http://localhost:8080/cliente/';
  urlProductos: string = 'http://localhost:8080/producto/lista';
  
  constructor(private http: HttpClient) { }

  postAuorizarUsuario(nombre:string, password:number): Observable<RepuestaAutorizacion>{
    let body = {
      nombre: nombre,
      password: password
    }
    return this.http.post<RepuestaAutorizacion>(this.urlAutorizacion, body);
  }

  getUsuario(id: string): Observable<Usuario>{
    return this.http.get<Usuario>(this.urlCliente + id);
  }

  getProductos(): Observable<ListaDeProductos>{
    return this.http.get<ListaDeProductos>(this.urlProductos);
  }
}
