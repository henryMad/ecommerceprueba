
import { Component, OnInit } from '@angular/core';
import { ListaDeProductos, ProductosCarrito } from 'src/app/interface/usuario';
import { InformacionEcommerceService } from 'src/app/services/informacion-ecommerce.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  estaElCarritoCreado: boolean = false;

  listaDeProductos: Array<ListaDeProductos> = [];
  carrito: Array<ListaDeProductos> = [];

  productosd: Array<ProductosCarrito> = [];


  constructor(private producto: InformacionEcommerceService) { }

  ngOnInit(): void {
    this.mostrarProductos();
  }

  mostrarProductos():void { 
    this.producto.getProductos().subscribe((respuesta:any) => {
      this.listaDeProductos.push(...respuesta);
    });
  }

  agregarProductos(lista: ListaDeProductos):void {
    this.carrito.push(lista);
  }

  eliminarProducto(id:number){
    this.carrito.slice(id, 0);
  }


  

}
