import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InformacionEcommerceService } from 'src/app/services/informacion-ecommerce.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  cliente = {
    nombre: '',
    password: 0,
  }

  constructor(
    private autorizacion: InformacionEcommerceService,
    private router: Router
  ) { }

  ngOnInit(): void {}

  autorizarUsuario():void {
    this.autorizacion.postAuorizarUsuario(this.cliente.nombre, this.cliente.password).
    subscribe(respuesta => {
      this.guardarIdEnLocalStorage(respuesta.id, respuesta.status);
    });
  }

  guardarIdEnLocalStorage(id: number, status:number):void {
    if(id !=null && id !=0 && status == 200){
      localStorage.setItem('id', id.toString());
      this.router.navigate(["/home"]);
    }
    else {
      alert("Contraseña incorrecta");
    }
  }

  loguearUsuario(): void {
    this.autorizarUsuario();
  }
}
