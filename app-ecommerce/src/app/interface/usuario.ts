export interface Usuario {
    id: number,
    nombreCliente:string;
    apellidoCliente: string;
    tipoCliente: string;
    fecha: Date
}

export interface RepuestaAutorizacion {
    id:number,
    status: number
}

export interface ListaDeProductos {
    id: number,
    nombre: string,
    stock: number,
    precio: number,
}

export interface ProductosCarrito {
    id: number,
    nombre: string,
    precio: number,
    cantidad: number,
}