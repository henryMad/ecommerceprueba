import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

import { InformacionEcommerceService } from 'src/app/services/informacion-ecommerce.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Output() estaCreado = new EventEmitter<boolean>();

  bandera:boolean = true;
   
  cliente = {
    nombreCliente: '',
    apellidoCliente: '',
    tipoCliente: ''
  }

  constructor(private usuario: InformacionEcommerceService, private router: Router) { 
    this.recuperarIdLocalStorage();
  }

  ngOnInit(): void {
    this.obtenerUsuario();
  }

  recuperarIdLocalStorage():any{
    let id = localStorage.getItem('id');
    if(id != null){
      return id;
    };
  }

   obtenerUsuario(){
    let id = this.recuperarIdLocalStorage();
    this.usuario.getUsuario(id).subscribe((respuesta) => {
      this.cliente = respuesta;
      console.log(respuesta);
    }); 
  }

  irLogin():void {
    this.router.navigate(["/login"]);
  }

  irHome(): void{
    this.router.navigate(["/home"]);
  }

  crearCarrito():void {
    this.bandera = true;
    this.estaCreado.emit(this.bandera);
  }
   
}
