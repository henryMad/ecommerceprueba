package com.ecommerce.models.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ecommerce.models.entity.Promociones;

public interface PromocionRespository extends JpaRepository<Promociones, Integer> {

}
