package com.ecommerce.models.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ecommerce.models.entity.FechasPromocionables;

public interface FechasPromocionableRepository extends JpaRepository<FechasPromocionables, Integer> {

}
