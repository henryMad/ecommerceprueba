package com.ecommerce.models.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ecommerce.models.entity.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Integer> {
	
}
