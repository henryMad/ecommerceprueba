package com.ecommerce.models.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;

import com.ecommerce.models.entity.Usuario;
import com.ecommerce.request.ClienteAutorizacionRequestDTO;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {
	
	Usuario findByNombreUsuarioAndPassword(String nombre, int password);
	
	Usuario findByPassword(int password);
}
