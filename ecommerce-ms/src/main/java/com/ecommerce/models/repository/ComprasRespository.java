package com.ecommerce.models.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ecommerce.models.entity.Compras;

public interface ComprasRespository extends JpaRepository<Compras, Integer> {

}
