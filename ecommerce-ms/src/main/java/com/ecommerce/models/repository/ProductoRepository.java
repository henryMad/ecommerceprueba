package com.ecommerce.models.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ecommerce.models.entity.Producto;

public interface ProductoRepository extends JpaRepository<Producto, Integer> {
	
}
