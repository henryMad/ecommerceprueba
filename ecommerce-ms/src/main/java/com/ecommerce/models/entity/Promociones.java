package com.ecommerce.models.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="promociones")
public class Promociones {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private int promocion;
	private float promocionTipo;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPromocion() {
		return promocion;
	}
	public void setPromocion(int promocion) {
		this.promocion = promocion;
	}
	public float getPromocionTipo() {
		return promocionTipo;
	}
	public void setPromocionTipo(float promocionTipo) {
		this.promocionTipo = promocionTipo;
	}
	
	
}
