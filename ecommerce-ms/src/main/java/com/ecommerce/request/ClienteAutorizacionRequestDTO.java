package com.ecommerce.request;

public class ClienteAutorizacionRequestDTO {
	
	private String nombre;
	private int password;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getPassword() {
		return password;
	}
	public void setPassword(int password) {
		this.password = password;
	}
	
}
