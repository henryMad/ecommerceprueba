package com.ecommerce.router.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.models.entity.Compras;
import com.ecommerce.models.entity.FechasPromocionables;
import com.ecommerce.models.entity.Producto;
import com.ecommerce.models.entity.Promociones;
import com.ecommerce.responseDTO.ResponseRegistroCompra;
import com.ecommerce.services.producto.ProductoService;

@CrossOrigin("*")
@RestController
@RequestMapping("/producto")
public class ControllerRouterProducto {
	
	
	@Autowired
	private ProductoService productoService;
	
	@GetMapping("/lista")
	public List<Producto> listarProductos() {
		return productoService.getProductos();
	}
	
	@GetMapping("/promocion")
	public List<Promociones> listarPromociones() {
		return productoService.getPromociones();
	}
	
	@GetMapping("/fechas-promocionables")
	public List<FechasPromocionables> fechasPromociones() {
		return productoService.getFechasPromociones();
	}
	
	@PostMapping("registrar-compra")
	public ResponseRegistroCompra registroCompra(@RequestBody List<Compras> compra) {
		List<Compras> data = productoService.registrarCompra(compra);
		ResponseRegistroCompra registro = new ResponseRegistroCompra();
		if(data != null) {
			return registro;
		}
		return new ResponseRegistroCompra();
	}
	
	

}
