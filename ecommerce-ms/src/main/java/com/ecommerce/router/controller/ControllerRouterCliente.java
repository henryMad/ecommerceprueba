package com.ecommerce.router.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.models.entity.Cliente;
import com.ecommerce.services.cliente.ClienteService;

@CrossOrigin("*")
@RestController
@RequestMapping("/cliente")
public class ControllerRouterCliente {

	@Autowired
	private ClienteService clienteService;
	
	@GetMapping("/{id}")
	public Optional<Cliente> cliente(@PathVariable int id) {
		return clienteService.getClienteById(id);
	}
}
