package com.ecommerce.router.controller;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.models.entity.Usuario;
import com.ecommerce.request.ClienteAutorizacionRequestDTO;
import com.ecommerce.responseDTO.ResponseAutorizacion;
import com.ecommerce.services.usuario.UsuarioService;

@CrossOrigin("*")
@RestController
@RequestMapping("/usuario")
public class ControllerRouterUsuario {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@PostMapping("/autorizacion")
	public ResponseAutorizacion autoirzacionUsuario(@RequestBody ClienteAutorizacionRequestDTO request) {
		Usuario dataUsuario = usuarioService.existeElUsuario(request);
		ResponseAutorizacion response = new ResponseAutorizacion();
		if(dataUsuario != null){
			response.setId(dataUsuario.getId());
			response.setStatus(200);
			return response;
		}
		return new ResponseAutorizacion();
	}
	
}	
