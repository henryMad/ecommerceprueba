package com.ecommerce.services.usuario;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.models.entity.Usuario;
import com.ecommerce.models.repository.UsuarioRepository;
import com.ecommerce.request.ClienteAutorizacionRequestDTO;

@Service
public class UsuarioService implements IUsuarioService {
	
	@Autowired 
	private UsuarioRepository clienteRepository;
	
	public Usuario existeElUsuario(ClienteAutorizacionRequestDTO request) {
		return clienteRepository.findByNombreUsuarioAndPassword(request.getNombre(), request.getPassword());	
	}
	
	public Usuario findByPassword(ClienteAutorizacionRequestDTO request) {
		return clienteRepository.findByPassword(request.getPassword());
	}
	
	public Optional<Usuario> getClienteById(int id){
		return clienteRepository.findById(id);
	}
}
