package com.ecommerce.services.usuario;

import java.util.Optional;

import com.ecommerce.models.entity.Usuario;
import com.ecommerce.request.ClienteAutorizacionRequestDTO;

public interface IUsuarioService {
	
	public Usuario existeElUsuario(ClienteAutorizacionRequestDTO request);
	
	public Usuario findByPassword(ClienteAutorizacionRequestDTO request);
	
	public Optional<Usuario> getClienteById(int id);
}
