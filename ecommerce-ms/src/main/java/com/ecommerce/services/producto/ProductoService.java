package com.ecommerce.services.producto;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.models.entity.Compras;
import com.ecommerce.models.entity.FechasPromocionables;
import com.ecommerce.models.entity.Producto;
import com.ecommerce.models.entity.Promociones;
import com.ecommerce.models.repository.ComprasRespository;
import com.ecommerce.models.repository.FechasPromocionableRepository;
import com.ecommerce.models.repository.ProductoRepository;
import com.ecommerce.models.repository.PromocionRespository;

@Service
public class ProductoService implements IProductoService {

	@Autowired
	private ProductoRepository productoRepository;
	
	@Autowired
	private PromocionRespository promocionRepository;
	
	@Autowired
	private FechasPromocionableRepository fechasPromocionablesRepository;
	
	@Autowired
	private ComprasRespository comprasRepository;
	
	
	public List<Producto> getProductos(){
		return productoRepository.findAll();
	}
	
	public List<Promociones> getPromociones(){
		return promocionRepository.findAll();
	}
	
	public List<FechasPromocionables> getFechasPromociones(){
		return fechasPromocionablesRepository.findAll();
	}
	
	public List<Compras> registrarCompra(List<Compras> compras) {
		return comprasRepository.saveAll(compras);
	}
	
	
	
	
	
}
