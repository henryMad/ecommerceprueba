package com.ecommerce.services.producto;

import java.util.List;

import com.ecommerce.models.entity.Compras;
import com.ecommerce.models.entity.FechasPromocionables;
import com.ecommerce.models.entity.Producto;
import com.ecommerce.models.entity.Promociones;

public interface IProductoService {
	public List<Producto> getProductos();
	public List<Promociones> getPromociones();
	public List<FechasPromocionables> getFechasPromociones();
	public List<Compras> registrarCompra(List<Compras> compras);
}
