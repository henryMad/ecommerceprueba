package com.ecommerce.services.cliente;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.models.entity.Cliente;
import com.ecommerce.models.repository.ClienteRepository;

@Service
public class ClienteService implements IClienteService {
	
	@Autowired
	private ClienteRepository clienteRepository;

	public Optional<Cliente> getClienteById(int id){
		return clienteRepository.findById(id);
	}
}
