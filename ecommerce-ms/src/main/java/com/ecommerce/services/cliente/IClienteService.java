package com.ecommerce.services.cliente;

import java.util.Optional;

import com.ecommerce.models.entity.Cliente;



public interface IClienteService {
	
	public Optional<Cliente> getClienteById(int id);
}
